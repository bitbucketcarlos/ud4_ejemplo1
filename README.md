# Ud4-Ejemplo1
_Ejemplo 1 de la Unidad 4._ 

Vamos a realizar _View Recycling_ haciendo uso de _RecyclerView_ para mostrar una lista con algunos de los vehículos (coches, naves, etc.) 
más famosos de películas y series. Se mostrará su nombre y la película o serie en la que apareció.

Para ello vamos a seguir los siguientes pasos:

## Paso 1: Creación de los _layouts_

Primero vamos a crear la lista usando _RecyclerView_ en el fichero _activity_main.xml_:
```html
<android.support.constraint.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerView"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</android.support.constraint.ConstraintLayout>
```
Después necesitamos crear otro layout ya que la lista va a contener dos _TextView_, 
uno para el nombre y otro para indicar dónde apareció, cada uno con su _id_.

_elementos_lista.xml_:
```
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:orientation="vertical"
    android:padding="16dp">

    <TextView
        android:id="@+id/nombreTextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="nombre"/>

    <TextView
        android:id="@+id/aparicionTextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="aparición"/>

</LinearLayout>
```

Observad que se utiliza el atributo _text_ del espacio de nombres _tools_ para ver cómo queda el diseño.

## Paso 2: Creación de la clase Vehículo

Como cada elemento de la lista va a contener más de un dato relacionado con el vehículo, vamos a crear una clase con dos atributos, uno para el nombre y otro para la aparición.

_Vehiculo.kt_:
```java
data class Vehiculo (var nombre: String, var aparicion: String)
```

## Paso 3: Creación del adaptador _VehiculoAdapter_

El siguiente paso es crear el adaptador que le pasará las _Views_ a _RecyclerView_. 
Éste heredará de _RecyclerView.Adapter_ de tipo _VehiculoAdapter.MiViewHolder_ donde _MiViewHolder_ es una _Inner class_ que nos
crearemos dentro de la clase _VehiculoAdapter_, esta clase solo contendrá los _TextViews_ de cada elemento de la lista.

Además, dentro de la clase _VehiculoAdapter_ tendremos que sobreescribir las siguientes funciones:
+ _onCreateViewHolder_: Construye un nuevo _ViewHolder_ con las nuevas _Views_ recicladas, en este caso "inflándolas" desde _elementos_lista.xml_.
+ _onBindViewHolder_: Muestra el elemento de la lista creado anteriormente (las _Views_ recicladas con los nuevos valores).
+ _getItemCount_: Devuelve el número total de elementos de la lista.

_VehiculoAdapter.kt_:
```java
class VehiculoAdapter (private val lista: ArrayList<Vehiculo>): RecyclerView.Adapter<VehiculoAdapter.MiViewHolder>() {

    // Creamos nuestro propio ViewHolder
    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nombretextView: TextView
        val apariciontextView: TextView

        init {
            // Buscamos los atributos del vehículo
            nombretextView = view.findViewById(R.id.nombreTextView)
            apariciontextView = view.findViewById(R.id.aparicionTextView)
        }
    }

    // Creamos nuevas views inflando el layout "elementos_lista"
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_lista, viewGroup, false)

        return MiViewHolder(view)
    }

    // Establecemos el nombre y la aparición para el vehículo de esa posición
    override fun onBindViewHolder(viewHolder: MiViewHolder, position: Int) {
        viewHolder.nombretextView.text = lista[position].nombre
        viewHolder.apariciontextView.text = lista[position].aparicion
    }

    // Devolvemos el tamaño de la lista de Vehículos
    override fun getItemCount() = lista.size
}
```
## Paso 4: Implementación de la clase _MainActivity_

El último paso es implementar la clase _MainActivity_ donde incluimos el _ArrayList_ de tipo _Vehiculo_ y añadimos sus elementos, 
buscamos el _RecyclerView_, indicamos que su tamaño es fijo y añadimos la línea divisoria entre elementos. 
Después creamos un _LinearLayout_ que contendrá cada elemento del _RecyclerView_ y por último creamos el _adapter_ y lo asignamos al _RecyclerView_.

_MainActivity.kt_:
```java
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Creamos el array de vehiculos.
        val vehiculos = ArrayList<Vehiculo>()

        vehiculos.add(Vehiculo("Ecto1", "Los cazafantasmas"))
        vehiculos.add(Vehiculo("DeLorean", "Regreso al futuro"))
        vehiculos.add(Vehiculo("Kitt", "El coche fantástico"))
        vehiculos.add(Vehiculo("Halcón Milenario", "Star Wars"))
        vehiculos.add(Vehiculo("Planet Express", "Futurama"))
        vehiculos.add(Vehiculo("TARDIS", "Doctor Who"))
        vehiculos.add(Vehiculo("USS Enterprise", "Star Trek"))
        vehiculos.add(Vehiculo("Nabucodonosor", "Matrix"))
        vehiculos.add(Vehiculo("Odiseus", "Ulises 31"))
        vehiculos.add(Vehiculo("Nostromo", "Alien"))

        // Buscamos el RecyclerView e indicamos que su tamaño es fijo
        val recycler = binding.recyclerView

        recycler.setHasFixedSize(true)

        // Añadimos la línea de separación de elementos de la lista
        // 0 para horizontal y 1 para vertical
        recycler.addItemDecoration(DividerItemDecoration(this, 1))

        // Asignamos un LinearLayout que contendrá cada elemento del RecyclerView
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        // Asignamos el adapter al RecyclerView
        recycler.adapter = VehiculoAdapter(vehiculos)
    }
}
```